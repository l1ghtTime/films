import React from 'react';
import Registration from './Components/Forms/Registration/Registration';
import { Route, Switch } from 'react-router-dom';
import MainArea from './Components/MainArea/MainArea';
import Authorization from './Components/Forms/Authorization/Authorization';



function App() {
  return (
    <React.Fragment>
      <Switch>
        <Route path='/registration' component={Registration} />
        <Route path='/authorization' component={Authorization} />
        <Route path='/' component={MainArea} />
      </Switch>
    </React.Fragment>
  );
}

export default App;
