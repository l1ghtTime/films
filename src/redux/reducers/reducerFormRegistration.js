import { FORM_REGISTRATION_STATUS, GET_REGISTRATION_NAME, GET_REGISTRATION_SERNAME, GET_REGISTRATION_EMAIL, GET_REGISTRATION_MALE, GET_REGISTRATION_FEMALE, GET_REGISTRATION_GENDER_STATUS, GET_REGISTRATION_AGE, GET_REGISTRATION_PASS, GET_REGISTRATION_REPEAT_PASS } from "../actions/actionTypes";


const initialState = {
    isRegistrated: false,
    isRegistrationName: '',
    isRegistrationSername: '',
    isRegistrationEmail: '',
    isRegistrationMale: '',
    isRegistrationFemale: '',
    isRegistrationGenderStatus: '',
    isRegistrationAge: '',
    isRegistrationPass: '',
    isRegistrationGenderRepeatPass: '',
}

const reducerFormRegistration = (state = initialState, action) => {

    switch (action.type) {

        case FORM_REGISTRATION_STATUS:
            return {
                ...state,
                isRegistrated: !state.isRegistrated
            }


        case GET_REGISTRATION_NAME:
            return {
                ...state,
                isRegistrationName: action.payload
            }

        case GET_REGISTRATION_SERNAME:
            return {
                ...state,
                isRegistrationSername: action.payload
            }

        case GET_REGISTRATION_EMAIL:
            return {
                ...state,
                isRegistrationEmail: action.payload
            }

        case GET_REGISTRATION_MALE:
            return {
                ...state,
                isRegistrationMale: action.payload
            }

        case GET_REGISTRATION_FEMALE:
            return {
                ...state,
                isRegistrationFemale: action.payload
            }

        case GET_REGISTRATION_GENDER_STATUS:
            return {
                ...state,
                isRegistrationGenderStatus: action.payload
            }

        case GET_REGISTRATION_AGE:
            return {
                ...state,
                isRegistrationAge: action.payload
            }

        case GET_REGISTRATION_PASS:
            return {
                ...state,
                isRegistrationPass: action.payload
            }

        case GET_REGISTRATION_REPEAT_PASS:
            return {
                ...state,
                isRegistrationGenderRepeatPass: action.payload
            }


        default:
            return state;
    }

};

export default reducerFormRegistration;