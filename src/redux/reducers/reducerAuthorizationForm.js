import { FORM_AUTHORIZATION_STATUS, FORM_AUTHORIZATION_MAIL, FORM_AUTHORIZATION_PASS, } from "../actions/actionTypes";

const initialState = {
    isAuthorized: false,
    isAuthorizationMail: '',
    isAuthorizationPass: '',
}

const reducerAuthorizationForm = (state = initialState, action) => {

    switch (action.type) {

        case FORM_AUTHORIZATION_STATUS:
            return {
                ...state,
                isAuthorized: action.payload
            }

        case FORM_AUTHORIZATION_MAIL:
            return {
                ...state,
                isAuthorizationMail: action.payload
            }

        case FORM_AUTHORIZATION_PASS:
            return {
                ...state,
                isAuthorizationPass: action.payload
            }

        default:
            return state;
    }

};

export default reducerAuthorizationForm;