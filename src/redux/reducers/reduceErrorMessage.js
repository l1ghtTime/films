import { ERROR_MESSAGE, } from "../actions/actionTypes";


const initialState = {
    errorMessage: false,
}

const reducerErrorMessage = (state = initialState, action) => {

    switch (action.type) {

        case ERROR_MESSAGE:
            return {
                ...state,
                errorMessage: action.payload
            }

        default:
            return state;
    }

};

export default reducerErrorMessage;