import { GET_IMAGE_URL, GET_FILM_TITLE, GET_FILM_DATA_REALISE, GET_FILM_DESCRIPTION, GET_FILM_INFO_POST, } from "../actions/actionTypes";


const initialState = {
    filmImageUrl: null,
    filmTitle: null,
    filmDataRelease: null,
    filmDescription: null,
    filmInfoPost: null,
}

const reducerFilmInfo = (state = initialState, action) => {

    switch (action.type) {

        case GET_IMAGE_URL:
            return {
                ...state,
                filmImageUrl: action.payload
            }

        case GET_FILM_TITLE:
            return {
                ...state,
                filmTitle: action.payload
            }

        case GET_FILM_DATA_REALISE:
            return {
                ...state,
                filmDataRelease: action.payload
            }

        case GET_FILM_DESCRIPTION:
            return {
                ...state,
                filmDescription: action.payload
            }


        case GET_FILM_INFO_POST:
            return {
                ...state,
                filmInfoPost: action.payload
            }

        default:
            return state;
    }

};

export default reducerFilmInfo;