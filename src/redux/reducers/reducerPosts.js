import { FETCH_POSTS, } from "../actions/actionTypes";


const initialState = {
    fetchedPosts: null,
}

const reducerPosts = (state = initialState, action) => {

    switch (action.type) {

        case FETCH_POSTS:
            return {
                ...state,
                fetchedPosts: action.payload
            }

        default:
            return state;
    }

};

export default reducerPosts;