import { combineReducers } from 'redux';
import reducerPosts from './reducers/reducerPosts';
import reducerFilmInfo from './reducers/reducerFilmInfo';
import reducerFormRegistration from './reducers/reducerFormRegistration';
import reducerAuthorizationForm from './reducers/reducerAuthorizationForm';
import reducerErrorMessage from './reducers/reduceErrorMessage';




export default combineReducers({
    reducerPosts, reducerFilmInfo, reducerFormRegistration, reducerAuthorizationForm, reducerErrorMessage, 
});