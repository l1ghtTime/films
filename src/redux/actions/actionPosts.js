import { FETCH_POSTS, } from "../actions/actionTypes";


export function addPostsData(value) {
    return {
        type: FETCH_POSTS,
        payload: value,
    }
}
