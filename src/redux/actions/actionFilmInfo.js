import { GET_IMAGE_URL, GET_FILM_TITLE, GET_FILM_DATA_REALISE, GET_FILM_DESCRIPTION, GET_FILM_INFO_POST } from "../actions/actionTypes";


export function addFilmUrl(value) {
    return {
        type: GET_IMAGE_URL,
        payload: value,
    }
}


export function addFilmTitle(value) {
    return {
        type: GET_FILM_TITLE,
        payload: value,
    }
}


export function addFilmDataRelease(value) {
    return {
        type: GET_FILM_DATA_REALISE,
        payload: value,
    }
}

export function addFilmDescription(value) {
    return {
        type: GET_FILM_DESCRIPTION,
        payload: value,
    }
}

export function addFilmInfoPost(value) {
    return {
        type: GET_FILM_INFO_POST,
        payload: value,
    }
}
