import { ERROR_MESSAGE, } from "../actions/actionTypes";


export function addErrorMessage(value) {
    return {
        type: ERROR_MESSAGE,
        payload: value,
    }
}
