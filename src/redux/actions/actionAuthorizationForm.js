import { FORM_AUTHORIZATION_STATUS, FORM_AUTHORIZATION_MAIL, FORM_AUTHORIZATION_PASS, } from "./actionTypes";


export function addFormAuthorizationStatus(value) {
    return {
        type: FORM_AUTHORIZATION_STATUS,
        payload: value
    }
}

export function addFormAuthorizationMail(value) {
    return {
        type: FORM_AUTHORIZATION_MAIL,
        payload: value
    }
}

export function addFormAuthorizationPass(value) {
    return {
        type: FORM_AUTHORIZATION_PASS,
        payload: value
    }
}
