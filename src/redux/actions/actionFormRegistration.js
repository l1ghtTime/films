import { FORM_REGISTRATION_STATUS, GET_REGISTRATION_NAME, GET_REGISTRATION_SERNAME, GET_REGISTRATION_EMAIL, GET_REGISTRATION_MALE, GET_REGISTRATION_FEMALE, GET_REGISTRATION_GENDER_STATUS, GET_REGISTRATION_AGE, GET_REGISTRATION_PASS, GET_REGISTRATION_REPEAT_PASS, } from "./actionTypes";


export function addFormRegistrationStatus() {
    return {
        type: FORM_REGISTRATION_STATUS,
    }
}


export function addFormRegistrationName(value) {
    return {
        type: GET_REGISTRATION_NAME,
        payload: value
    }
}


export function addFormRegistrationSername(value) {
    return {
        type: GET_REGISTRATION_SERNAME,
        payload: value
    }
}


export function addFormRegistrationEmail(value) {
    return {
        type: GET_REGISTRATION_EMAIL,
        payload: value
    }
}

export function addFormRegistrationMale(value) {
    return {
        type: GET_REGISTRATION_MALE,
        payload: value
    }
}

export function addFormRegistrationFemale(value) {
    return {
        type: GET_REGISTRATION_FEMALE,
        payload: value
    }
}

export function addFormRegistrationGenderStatus(value) {
    return {
        type: GET_REGISTRATION_GENDER_STATUS,
        payload: value
    }
}

export function addFormRegistrationAge(value) {
    return {
        type: GET_REGISTRATION_AGE,
        payload: value
    }
}

export function addFormRegistrationPass(value) {
    return {
        type: GET_REGISTRATION_PASS,
        payload: value
    }
}

export function addFormRegistrationRepeatPass(value) {
    return {
        type: GET_REGISTRATION_REPEAT_PASS,
        payload: value
    }
}





