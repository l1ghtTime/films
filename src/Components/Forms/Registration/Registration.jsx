import React from 'react';
import '../Forms.sass';
import { addFormRegistrationStatus, addFormRegistrationName, addFormRegistrationSername, addFormRegistrationEmail, addFormRegistrationMale, addFormRegistrationFemale, addFormRegistrationGenderStatus, addFormRegistrationAge, addFormRegistrationPass, addFormRegistrationRepeatPass } from '../../../redux/actions/actionFormRegistration';
import { useDispatch, useSelector } from 'react-redux';
import { addFormAuthorizationStatus } from '../../../redux/actions/actionAuthorizationForm';
import { addErrorMessage } from '../../../redux/actions/actionErrorMessage';
import { useHistory } from 'react-router-dom';


const Registration = () => {

    const dispatch = useDispatch();
    const registrationState = useSelector(state => state.reducerFormRegistration) 
    const authorizationState = useSelector(state => state.reducerAuthorizationForm) 
    const ErrorMess = useSelector(state => state.reducerErrorMessage) 

    const history = useHistory();


    console.log(authorizationState);
    function handleActionName(event) {
        dispatch(addFormRegistrationName(event.target.value));
    }

    function handleActionSername(event) {
        dispatch(addFormRegistrationSername(event.target.value));
    }

    function handleActionEmail(event) {
        dispatch(addFormRegistrationEmail(event.target.value));
    }

    function handleActionMale(event) {

        const parent = event.target.parentNode;
        if(event.target.value === "on") {
            dispatch(addFormRegistrationMale(parent.children[1].innerHTML));
            dispatch(addFormRegistrationGenderStatus(parent.children[1].innerHTML));
        }
    }

    function handleActionFemale(event) {

        const parent = event.target.parentNode;
        if(event.target.value === "on") {
            dispatch(addFormRegistrationFemale(parent.children[1].innerHTML));
            dispatch(addFormRegistrationGenderStatus(parent.children[1].innerHTML));
        }
    }

    function handleActionAge(event) {
        dispatch(addFormRegistrationAge(event.target.value));
    }

    function handleActionPass(event) {
        dispatch(addFormRegistrationPass(event.target.value));
    }

    function handleActionRepeatPass(event) {
        dispatch(addFormRegistrationRepeatPass(event.target.value));
    }


    function handleClick(event) {

        let regex = /@/g;
        
        event.preventDefault();

        if( registrationState.isRegistrationName.length > 0 &&
            registrationState.isRegistrationSername.length > 0 &&
            registrationState.isRegistrationEmail.length > 0 &&
            registrationState.isRegistrationGenderStatus.length > 0 &&
            registrationState.isRegistrationAge.length > 0 &&
            registrationState.isRegistrationPass.length > 0 &&
            registrationState.isRegistrationGenderRepeatPass.length > 0 &&
            registrationState.isRegistrationPass === registrationState.isRegistrationGenderRepeatPass) {

                localStorage.setItem("name", registrationState.isRegistrationName);    
                localStorage.setItem("surname", registrationState.isRegistrationSername);    
                localStorage.setItem("email", registrationState.isRegistrationEmail);    
                localStorage.setItem("gender", registrationState.isRegistrationGenderStatus);    
                localStorage.setItem("age", registrationState.isRegistrationAge);    
                localStorage.setItem("pass", registrationState.isRegistrationPass);    
                localStorage.setItem("repeat pass", registrationState.isRegistrationGenderRepeatPass); 

                dispatch(addFormRegistrationStatus());
                history.push('/allfilms');
            } 

            if(regex.test(registrationState.isRegistrationEmail) === false ||
               registrationState.isRegistrationPass !== registrationState.isRegistrationGenderRepeatPass ||
               !registrationState.isRegistrationName.length ||
               !registrationState.isRegistrationSername.length ||
               !registrationState.isRegistrationEmail.length ||
               !registrationState.isRegistrationGenderStatus.length ||
               !registrationState.isRegistrationAge.length ||
               !registrationState.isRegistrationPass.length ||
               !registrationState.isRegistrationGenderRepeatPass.length) {
                dispatch(addErrorMessage(true));

                setTimeout(() => {
                    dispatch(addErrorMessage(false));
                }, 2000);
            }

            dispatch(addFormAuthorizationStatus(true));



    }

    return (
        <div className="box">
            <form className="form">

                {ErrorMess.errorMessage === true 
                ? <div className="message">
                    <span className="message__text">Error</span>
                </div>
                : null}


                <div className="form-column w-100">

                    <div className="col">
                        <div className="form-group w-100">
                            <label htmlFor="inputNameRegistration" className="form__label">Имя:</label>
                            <input type="text" className="form__area form-control" id="inputNameRegistration" placeholder="Имя" autoComplete="off" required  onChange={handleActionName}/>
                        </div>
                    </div>

                    <div className="col">
                        <div className="form-group w-100">
                            <label htmlFor="inputSurnameRegistration" className="form__label">Фамилия:</label>
                            <input type="text" className="form__area form-control" id="inputSurnameRegistration" placeholder="Фамилия" autoComplete="off" required onChange={handleActionSername} />
                        </div>
                    </div>


                    <div className="col">
                        <div className="form-group w-100">
                            <label htmlFor="inputEmailRegistration" className="form__label">Email:</label>
                            <input type="email" className="form__area form-control" id="inputEmailRegistration" placeholder="Email" autoComplete="off" required onChange={handleActionEmail} />
                        </div>
                    </div>







                    <div className="col">
                        <div className="form-group w-100">


                            <div className="custom-control custom-radio form__radio">
                                <input type="radio" className="custom-control-input" id="maleRadioRegistration" name="radio-stacked" required onChange={handleActionMale}/>
                                <label className="custom-control-label form__label" htmlFor="maleRadioRegistration">М</label>
                            </div>

                            <div className="custom-control custom-radio form__radio">
                                <input type="radio" className="custom-control-input" id="femaleRadioRegistration" name="radio-stacked" required onChange={handleActionFemale} />
                                <label className="custom-control-label form__label" htmlFor="femaleRadioRegistration">Ж</label>
                            </div>

                        </div>
                    </div>



                    <div className="col">
                        <div className="form-group">
                            <label htmlFor="inputAgeRegistration" className="form__label">Сколько вам лет?</label>
                            <input type="text" className="form__area form-control" id="inputAgeRegistration" placeholder="Сколько вам лет?" autoComplete="off" required onChange={handleActionAge} />
                        </div>
                    </div>


                    <div className="col">
                        <div className="form-group">
                            <label htmlFor="InputPasswordRegistration" className="form__label">Пароль:</label>
                            <input type="password" className="form__area form-control" id="InputPasswordRegistration" placeholder="Пароль" autoComplete="off" required onChange={handleActionPass} />
                        </div>
                    </div>



                    <div className="col">
                        <div className="form-group">
                            <label htmlFor="CheckInputPasswordRegistration" className="form__label">Подтвердитe Пароль:</label>
                            <input type="password" className="form__area form-control" id="CheckInputPasswordRegistration" placeholder="Подтвердитe Пароль" autoComplete="off" required onChange={handleActionRepeatPass} />
                        </div>
                    </div>

                    <div className="col">
                        <button type="submit" className="form__btn btn w-100" onClick={handleClick}>Зарегистрироваться</button>
                    </div>


                </div>
            </form>
        </div>
    );
};

export default Registration;