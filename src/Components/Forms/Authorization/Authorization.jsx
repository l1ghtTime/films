import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { addFormAuthorizationStatus, addFormAuthorizationMail, addFormAuthorizationPass } from '../../../redux/actions/actionAuthorizationForm';
import { useHistory, Link } from 'react-router-dom';
import { addErrorMessage } from '../../../redux/actions/actionErrorMessage';


const Authorization = () => {


    const dispatch = useDispatch();
    const authorizationData = useSelector(state => state.reducerAuthorizationForm);
    const ErrorMess = useSelector(state => state.reducerErrorMessage) 
    const history = useHistory();

    function handleActionMail(event) {
        dispatch(addFormAuthorizationMail(event.target.value));
    }

    function handleActionPass(event) {
        dispatch(addFormAuthorizationPass(event.target.value));
    }

    function handleClick(event) {
        let regex = /@/g;

        if(authorizationData.isAuthorizationMail.length > 0 &&
           authorizationData.isAuthorizationPass.length > 0 &&  
           authorizationData.isAuthorizationMail === localStorage.getItem("email") &&
           authorizationData.isAuthorizationPass === localStorage.getItem("pass")) {
                dispatch(addFormAuthorizationStatus(true));
                history.push('/allfilms');
           } 

           if(regex.test(authorizationData.isAuthorizationMail) === false ||
              !authorizationData.isAuthorizationMail.length ||
              !authorizationData.isAuthorizationPass.length ||
              authorizationData.isAuthorizationMail.length !== localStorage.getItem("email") ||
              authorizationData.isAuthorizationMail !== localStorage.getItem("email")) {
                dispatch(addErrorMessage(true));

                setTimeout(() => {
                    dispatch(addErrorMessage(false));
                }, 2000);
              }
    }


    
    return (
        <div className="box">
            <form className="form form_authorization">

            {ErrorMess.errorMessage === true 
                ? <div className="message">
                    <span className="message__text">Error</span>
                </div>
                : null}
                <div className="form-column w-100">
                
                    <div className="col">
                        <div className="form-group w-100">
                            <label htmlFor="inputEmailRegistration" className="form__label">Email:</label>
                            <input type="email" className="form__area form-control" id="inputEmailRegistration" placeholder="Email" autoComplete="off" required  onChange={handleActionMail} />
                        </div>
                    </div>



                    <div className="col">
                        <div className="form-group w-100">
                            <label htmlFor="InputPasswordRegistration" className="form__label">Пароль:</label>
                            <input type="password" className="form__area form-control" id="InputPasswordRegistration" placeholder="Пароль" autoComplete="off" required onChange={handleActionPass} />
                        </div>
                    </div>


                    <div className="col">
                        <Link to="/registration" className="form__registrationlink">Registration</Link>
                    </div>

                    <div className="col">
                        <button type="submit" className="form__btn btn w-100" onClick={handleClick}>Войти</button>
                    </div>

                </div>
            </form>
        </div>
    );
}

export default Authorization;