import React from 'react';
import './ContentArea.sass';
import { useSelector } from 'react-redux';
import { Route, Switch } from 'react-router-dom';
import AllFilmsPage from './AllFilmsPage/AllFilmsPage';
import FilmInfoPage from './FilmInfoPage/FilmInfoPage';
import FavoritesFilms from './FavoritesFilms/FavoritesFilms';
import Profile from './Profile/Profile';


const ContentArea = () => {

    const posts = useSelector(state => state.reducerPosts.fetchedPosts);

    return (
        <div className="container">
            <div className="row">
                <div className="col-xl-12">
                    {posts ?
                        <div className="content-area">
                            <Switch>
                                <Route path="/allfilms" render={() => <AllFilmsPage />} />
                                <Route path="/filminfo" render={() => <FilmInfoPage />} />
                                <Route path="/favorites" render={() => <FavoritesFilms />} />
                                <Route path="/profile" render={() => <Profile />} />
                            </Switch>
                        </div>
                        : null}
                </div>
            </div>
        </div>
    );
};

export default ContentArea;