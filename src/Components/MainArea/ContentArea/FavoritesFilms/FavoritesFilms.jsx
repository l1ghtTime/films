import React from 'react';
import { Link } from 'react-router-dom';
import close from '../../../../icons/close_panel.png';



const FavoritesFilms = (props) => {

    let infoPosts = JSON.parse(localStorage.getItem("posts"));

    console.log("POSTS", infoPosts);

    function handleClickClose(event) {

        // infoPosts.filter(item => {
        //     // if(item.id !== id) {
        //     //     return console.log(item);
        //     // } 
            
        // })
    }

    return (
        <div className="container">
            <div className="row">
                <div className="col-xl-12">
                    <div className="favorites">
                        <ul className="favorites__title">
                            <li className="favorites__item">Название</li>
                            <li className="favorites__item">Постер</li>
                            <li className="favorites__item">Дата релиза</li>
                            <li className="favorites__item">Описание</li>
                        </ul>

                        {
                            infoPosts.map(posts => posts.map((post, index) => (
                                <ul className="favorites__list"  key={index} >
                                    <li className="favorites__data">

                                        <span className="favorites__info">
                                            <Link to="" className="favorites__link">{post.title}</Link>
                                        </span>
                                        <span className="favorites__info">
                                            {post.url}
                                        </span>
                                        <span className="favorites__info">{post.release}</span>
                                        <span className="favorites__info">{post.description}</span>

                                    </li>

                                    <div className="favorites__close" style={{ background: `url(${close})` }} onClick={handleClickClose}></div>
                                </ul>
                            )))
                        }
                    </div>
                </div>
            </div>
        </div>
    )
}

export default FavoritesFilms;

