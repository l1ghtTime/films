import React from 'react';
import { useHistory } from 'react-router-dom';
import { addFilmUrl, addFilmTitle, addFilmDataRelease, addFilmDescription } from '../../../../../redux/actions/actionFilmInfo';
import { useDispatch } from 'react-redux';

const Posts = ({ posts }) => {

    const history = useHistory();
    const dispatch = useDispatch();
    function handleClick(event) {
        const element = event.target.parentNode;
        const firstChildUrl = element.firstChild.src;
        const titleText = element.children[1].innerHTML;
        const releaseText = element.children[2].innerHTML;
        const filmOverview = element.children[3].innerHTML;

        dispatch(addFilmUrl(firstChildUrl));
        dispatch(addFilmTitle(titleText));
        dispatch(addFilmDataRelease(releaseText));
        dispatch(addFilmDescription(filmOverview));

        console.log("SECONS CHILD", element.children[1].innerHTML );

        history.push('/filminfo');
    } 

    return (
        <div className="container">
            <div className="row">
                <div className="col-xl-12">

                    <ul className="cards">
                        {posts.map(post => (
                            <li className="card" key={post.id} onClick={handleClick}>
                                <img src={`https://image.tmdb.org/t/p/w220_and_h330_face/${post.poster_path}`} alt="poster" className="card__image" />
                                <h5 className="card__title">{post.title}</h5>
                                <span style={{display: "none"}}>{post.release_date}</span>
                                <span style={{display: "none"}}>{post.overview}</span>
                            </li>
                        ))}
                    </ul>

                </div>
            </div>
        </div>
    );


}


export default Posts;
