import React, { useState } from 'react';
import Posts from './Posts/Posts';
import { Route } from 'react-router-dom';
import { useSelector } from 'react-redux';
import Pagination from './Pagination/Pagination';

const AllFilmsPage = () => {


    const posts = useSelector(state => state.reducerPosts.fetchedPosts);


    console.log("POSTS", posts);

    const [currentPage, setCurrentPage] = useState(1);
    const [postsPerPage] = useState(9);


    const indexOfLastPost = currentPage * postsPerPage;
    const indexOfFirstPost = indexOfLastPost - postsPerPage;


    const paginate = (pageNumber) => setCurrentPage(pageNumber);

    if (posts) {
        const currentPosts = posts.results.slice(indexOfFirstPost, indexOfLastPost);

        return (
            <React.Fragment>
                <Route path="/allfilms" render={() => <Posts posts={currentPosts} />}/>
                <Route path="/allfilms" render={() => <Pagination postsPerPage={postsPerPage} totalPosts={posts.results.length} paginate={paginate} />}/>
            </React.Fragment>
        );
    } else {
        return null;
    }

};

export default AllFilmsPage;