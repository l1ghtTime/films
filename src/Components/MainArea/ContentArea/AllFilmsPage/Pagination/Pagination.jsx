import React from 'react';
import { Link, useHistory } from 'react-router-dom';

const Pagination = ({ postsPerPage, totalPosts, paginate }) => {

    const history = useHistory();

    const pageNumbers = [];

    let pageCount = Math.ceil(totalPosts / postsPerPage);

    for (let i = 1; i <= pageCount; i++) {
        pageNumbers.push(i);
    }

    console.log("HISTORY", history);

    return (
            <ul className="pagination">
                {pageNumbers.map(number => (
                    <li key={number} className="pagination__item page-item">
                        <Link to="/allfilms" className="pagination__link page-link" onClick={() => paginate(number)}>
                            {number}
                        </Link>
                    </li>
                ))}
            </ul>
    )
}

export default Pagination;
