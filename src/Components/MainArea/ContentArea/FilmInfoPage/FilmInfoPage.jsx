import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { addFilmInfoPost } from '../../../../redux/actions/actionFilmInfo';



const FilmInfoPage = () => {

    const dispatch = useDispatch();
    const filmData = useSelector(state => state.reducerFilmInfo);
    const authorizationData = useSelector(state => state.reducerAuthorizationForm);
    const history = useHistory();

    const filmInfoArray = [];


    function handleClick(){
        if(authorizationData.isAuthorized === true) {
            history.push('/favorites');
            const newArray = filmInfoArray.concat({"title": filmData.filmTitle, "url": filmData.filmImageUrl, "release": filmData.filmDataRelease, "description": filmData.filmDescription, });
        
            let postsInfo = [];

            postsInfo.push(newArray);

            

    
            if(!localStorage.getItem("posts")) {
                localStorage.setItem("posts", `${JSON.stringify(postsInfo)}`)
            }else {
                if(newArray.length) {
                    dispatch(addFilmInfoPost(newArray));
                    let infoPosts = JSON.parse(localStorage.getItem("posts"));
                    localStorage.setItem("posts", `${JSON.stringify(infoPosts.concat(postsInfo))}`)
                }
            }
            



        } else {
            history.push('/authorization');
        }


    }
    console.log("POSTS", localStorage.getItem("posts"));

    return (
        <div className="container">
            <div className="row">
                <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                    <div className="film-info">
                        <div className="film-info__leftbar">
                            <div className="film-info__box">

                                <img src={filmData.filmImageUrl} alt="poster" className="film-info__image" />
                                <button className="film-info__btn" onClick={handleClick}>Добавить в избранное</button>

                            </div>
                        </div>

                        <div className="film-info__rightbar">
                            <h2 className="film-info__title">{filmData.filmTitle}</h2>
                            <div className="film-info__line">
                                <span className="film-info__text">Release: </span>
                                <span className="film-info__release">{filmData.filmDataRelease}</span>
                            </div>
                           
                            <div className="film-info__line">
                                <span className="film-info__text">About film:</span>
                                <span className="film-info__overview">{filmData.filmDescription}</span>
                            </div>

                        </div>


                    </div>
                </div>
            </div>
        </div>
    )
}

export default FilmInfoPage;
