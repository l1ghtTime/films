import React from 'react';
import Header from './Header/Header';
import ContentArea from './ContentArea/ContentArea';
import { Route, Switch } from 'react-router-dom';


const MainArea = () => {
    return (
        <React.Fragment>
            <Header />
            <Switch>
                <Route path="/allfilms" render={() => <ContentArea />} />
                <Route path="/filminfo" render={() => <ContentArea />} />
                <Route path="/favorites" render={() => <ContentArea />} />
            </Switch>

        </React.Fragment>
    );
};

export default MainArea;