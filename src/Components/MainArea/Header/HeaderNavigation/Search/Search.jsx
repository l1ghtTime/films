import React, {useState } from 'react';
import { addPostsData } from '../../../../../redux/actions/actionPosts';
import { useHistory } from 'react-router-dom';
import { useDispatch } from 'react-redux';


const Search = () => {

    const history = useHistory();

    const dispatch = useDispatch();

    const [searchValue, setSearchValue] = useState();

    async function fetchPosts(title) {
        let response = await fetch(`https://api.themoviedb.org/3/search/movie?api_key=9c372210a602cdd9f16b5a68572358e9&language=en-US&query=${title}&page=1&include_adult=false`)
        response = await response.json()
        dispatch(addPostsData(response));
    }

    function hanleClick() {
        fetchPosts(searchValue);
        history.push("/allfilms");
    }

    return (
        <div className="navigation__search">
            <input type="text" autoComplete="off" className="navigation__area" onChange={event => setSearchValue(event.target.value)} />
            <button type="button" className="navigation__btn" onClick={hanleClick}></button>
        </div>
    );
}

export default Search;