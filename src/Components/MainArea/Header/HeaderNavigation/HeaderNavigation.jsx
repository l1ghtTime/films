import React from 'react';
import { Link, useHistory } from 'react-router-dom';
import Search from './Search/Search';
import { useSelector, useDispatch } from 'react-redux';
import { addFormAuthorizationStatus } from '../../../../redux/actions/actionAuthorizationForm';



const HeaderNavigation = () => {

    
    const dispatch = useDispatch();
    const registrationState = useSelector(state => state.reducerFormRegistration) 
    const authorizationState = useSelector(state => state.reducerAuthorizationForm) 
    const history = useHistory();

    

    function handleClick() {
        dispatch(addFormAuthorizationStatus(false));
    }


    function handleClickProfile() {
        history.push("/profile");
    }

    console.log('AUTHORIZATION', registrationState.isAuthorized);
    return (
        <nav className="header__navigation navigation">

            {authorizationState.isAuthorized === true
             ? <span className="navigation__user"  style={{cursor:"pointer"}} onClick={handleClickProfile}>{`${localStorage.getItem("name")} ${localStorage.getItem("surname")}`}</span>
             : <span className="navigation__user">Гость</span>
            }

            {authorizationState.isAuthorized === true

           ? <Link to="/authorization" className="navigation__link" onClick={handleClick}>
                Выйти
            </Link>

           : <Link to="/authorization" className="navigation__link">
                Войти
            </Link>

            }
            <Link to="/registration" className="navigation__link">
                Регистрация
            </Link>


            <Search />
        </nav>
    );
};

export default HeaderNavigation;