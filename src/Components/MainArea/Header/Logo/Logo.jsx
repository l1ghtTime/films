import React from 'react';
import { Link } from 'react-router-dom';

const Logo = () => {
    return (
        <nav className="header__logo">
            <Link to="/" className="logo">
                Films
            </Link>  
        </nav>
    );
}

export default Logo;