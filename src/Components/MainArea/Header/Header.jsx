import React from 'react';
import '../Header/Header.sass';
import Logo from './Logo/Logo';
import HeaderNavigation from './HeaderNavigation/HeaderNavigation';
const Header = () => {
    return (
        <div className="header">
            <div className="container">
                <div className="row flex-column flex-sm-column flex-md-row flex-lg-row flex-xl-row">
                    <div className="col-2 col-sm-2 col-md-2 col-lg-2 col-xl-2">
                        <Logo />
                    </div>

                    <div className="col-4 col-sm-10 offset-sm-0 col-md-10 offset-md-0 col-lg-8 offset-lg-2 col-xl-7 offset-xl-3">
                        <HeaderNavigation />
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Header;